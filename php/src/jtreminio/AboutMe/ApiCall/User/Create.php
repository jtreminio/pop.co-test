<?php

namespace jtreminio\AboutMe\ApiCall\User;

use jtreminio\AboutMe;
use jtreminio\AboutMe\Entity;

class Create implements AboutMe\ApiCallInterface
{
    /** @var Entity\User\Create\Request */
    private $request;

    /** @var Entity\User\Create\Response */
    private $response;

    /** @var string */
    private $url = 'https://api.about.me/api/v2/json/user/create/';

    public function __construct(Entity\User\Create\Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        $values = $this->request->toArray();

        return $this->url . $values['username'];
    }

    /**
     * @return array
     */
    public function getData()
    {
        $data = $this->request->toArray();

        $data['login'] = 'true';

        unset($data['username']);

        return $data;
    }

    /**
     * @param array $response
     * @return $this
     */
    public function parseResponse(array $response)
    {
        $this->response = new Entity\User\Create\Response($response);

        return $this;
    }

    /**
     * @return Entity\User\Create\Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return bool
     */
    public function callFailed()
    {
        return !empty($this->response->getErrorMessage());
    }
}
