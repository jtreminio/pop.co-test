<?php

namespace jtreminio\AboutMe\ApiCall\User;

use jtreminio\AboutMe;
use jtreminio\AboutMe\Entity;

class Login implements AboutMe\ApiCallInterface
{
    /** @var Entity\User\Login\Request */
    private $request;

    /** @var Entity\User\Login\Response */
    private $response;

    /** @var string */
    private $url = 'https://api.about.me/api/v2/json/user/login/';

    public function __construct(Entity\User\Login\Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        $values = $this->request->toArray();

        return $this->url . $values['username'];
    }

    /**
     * @return array
     */
    public function getData()
    {
        $data = $this->request->toArray();

        unset($data['username']);

        return $data;
    }

    /**
     * @param array $response
     * @return $this
     */
    public function parseResponse(array $response)
    {
        $this->response = new Entity\User\Login\Response($response);

        return $this;
    }

    /**
     * @return Entity\User\Login\Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return bool
     */
    public function callFailed()
    {
        return !empty($this->response->getErrorMessage());
    }
}
