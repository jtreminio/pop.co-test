<?php

namespace jtreminio\AboutMe\Entity;

interface ArraySerializable
{
    public function fromArray(array $values);
    public function toArray();
}
