<?php

namespace jtreminio\AboutMe\Entity;

abstract class ArraySerializableAbstract
{
    /**
     * @param array $values
     * @return $this
     */
    public function fromArray(array $values)
    {
        foreach ($values as $key => $value) {
            $key = str_replace(' ', '', strtolower(trim($key)));

            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }
}
