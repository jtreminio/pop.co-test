<?php

namespace jtreminio\AboutMe\Entity\User\Login;

use jtreminio\AboutMe;
use jtreminio\AboutMe\Functions;

class Request extends AboutMe\Entity
{
    protected $username;

    /**
     * @var string Must be set to ‘password’
     */
    protected $grant_type = 'password';

    /**
     * @var string Scope of the access token requested: mobile or general
     */
    protected $scope = 'general';

    /**
     * @var string The password for the user
     */
    protected $password;

    /**
     * @var string If true, returns the user’s page information on successful login
     */
    protected $show_profile = 'false';

    /**
     * @var string Strips or includes HTML from the returned bio
     */
    protected $strip_html = 'true';

    /**
     * @var string Identifier that you provide, to be returned in the JSON object
     */
    protected $requestid;

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @param string $grant_type
     * @return $this
     */
    public function setGrantType($grant_type)
    {
        $this->grant_type = $grant_type;

        return $this;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param string $requestid
     * @return $this
     */
    public function setRequestid($requestid)
    {
        $this->requestid = $requestid;

        return $this;
    }

    /**
     * @param string $scope
     * @return $this
     */
    public function setScope($scope)
    {
        $this->scope = $scope;

        return $this;
    }

    /**
     * @param mixed $show_profile
     * @return $this
     */
    public function setShowProfile($show_profile)
    {
        $this->show_profile = Functions::boolStringValue($show_profile);

        return $this;
    }

    /**
     * @param mixed $strip_html
     * @return $this
     */
    public function setStripHtml($strip_html)
    {
        $this->strip_html = Functions::boolStringValue($strip_html);

        return $this;
    }
}
