<?php

namespace jtreminio\AboutMe\Entity\User\Login;

use jtreminio\AboutMe;

class Response extends AboutMe\Entity implements AboutMe\EntityResponseInterface
{
    /**
     * @var string Status code for the call; 200 if successful
     */
    protected $status;

    /**
     * @var string Access token if login=true is provided
     */
    protected $access_token;

    /**
     * @var string Number of seconds until the token expires.
     *
     * If not provided, the token does not expire
     */
    protected $expires_in;

    /**
     * @var Response\UserInfo
     */
    protected $user_info;

    /**
     * @var string Reason call failed if status is not 200
     */
    protected $error_message;

    public function __construct(array $values = [])
    {
        if (!empty($values['user_info'])) {
            $user = $values['user_info'];
            unset($values['user_info']);

            $this->user_info = new Response\UserInfo($user);
        }

        parent::__construct($values);
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->access_token;
    }

    /**
     * @return string
     */
    public function getExpiresIn()
    {
        return $this->expires_in;
    }

    /**
     * @return Response\UserInfo
     */
    public function getUserInfo()
    {
        return $this->user_info;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->error_message;
    }
}
