<?php

namespace jtreminio\AboutMe\Entity\User\Login\Response;

use jtreminio\AboutMe;

class Website extends AboutMe\Entity
{
    /**
     * @var string
     */
    protected $display_name;

    /**
     * @var string
     */
    protected $icon_url;

    /**
     * @var string
     */
    protected $my_collections;

    /**
     * @var string
     */
    protected $site_url;

    /**
     * @var string
     */
    protected $modal_url;

    /**
     * @var string
     */
    protected $platform;

    /**
     * @var string
     */
    protected $service_url;

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->display_name;
    }

    /**
     * @return string
     */
    public function getIconUrl()
    {
        return $this->icon_url;
    }

    /**
     * @return string
     */
    public function getModalUrl()
    {
        return $this->modal_url;
    }

    /**
     * @return string
     */
    public function getMyCollections()
    {
        return $this->my_collections;
    }

    /**
     * @return string
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @return string
     */
    public function getServiceUrl()
    {
        return $this->service_url;
    }

    /**
     * @return string
     */
    public function getSiteUrl()
    {
        return $this->site_url;
    }
}
