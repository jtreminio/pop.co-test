<?php

namespace jtreminio\AboutMe\Entity\User\Login\Response;

use jtreminio\AboutMe;

class UserInfo extends AboutMe\Entity
{
    /**
     * @var string
     */
    protected $profile;

    /**
     * @var string
     */
    protected $bio;

    /**
     * @var string
     */
    protected $display_name;

    /**
     * @var string
     */
    protected $user_name;

    /**
     * @var Website[]
     */
    protected $websites;

    /**
     * @var array[AvailableBackgrounds]
     */
    protected $available_backgrounds;

    /**
     * @var string
     */
    protected $email_address;

    public function __construct(array $values = [])
    {
        if (!empty($values['websites'])) {
            $websites = $values['websites'];
            unset($values['websites']);

            foreach ($websites as $website) {
                $this->websites[] = new Website($website);
            }
        }

        if (!empty($values['available_backgrounds'])) {
            $availableBackgrounds = $values['available_backgrounds'];
            unset($values['available_backgrounds']);

            foreach ($availableBackgrounds as $type => $collection) {
                foreach ($collection as $background) {
                    $this->available_backgrounds[$type][] = new AvailableBackground($background);
                }
            }
        }

        parent::__construct($values);
    }

    /**
     * @return string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->display_name;
    }

    /**
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->email_address;
    }

    /**
     * @return string
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->user_name;
    }

    /**
     * @return Website[]
     */
    public function getWebsites()
    {
        return $this->websites;
    }

    /**
     * @return array[AvailableBackgrounds]
     */
    public function getAvailableBackgrounds()
    {
        return $this->available_backgrounds;
    }
}
