<?php

namespace jtreminio\AboutMe\Entity\User\Login\Response;

use jtreminio\AboutMe;

class AvailableBackground extends AboutMe\Entity
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $artist;

    /**
     * @var string
     */
    protected $image_name;

    /**
     * @var string
     */
    protected $image_url;

    /**
     * @var string
     */
    protected $artist_url;

    /**
     * @var string
     */
    protected $thumb_url;

    /**
     * @return string
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * @return string
     */
    public function getArtistUrl()
    {
        return $this->artist_url;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->image_name;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return $this->image_url;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getThumbUrl()
    {
        return $this->thumb_url;
    }
}
