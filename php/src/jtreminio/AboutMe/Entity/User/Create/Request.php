<?php

namespace jtreminio\AboutMe\Entity\User\Create;

use jtreminio\AboutMe;
use jtreminio\AboutMe\Functions;

class Request extends AboutMe\Entity
{
    /**
     * @var string Requested username. Maximum length is 32 characters
     */
    protected $username;

    /**
     * @var string Email address to associate with this account.
     *
     * Must be a valid email address and from a valid domain
     */
    protected $email;

    /**
     * @var string The password to set on the account
     */
    protected $password;

    /**
     * @var string First name of the user, up to 32 characters
     */
    protected $first_name;

    /**
     * @var string Last name of the user, up to 32 characters
     */
    protected $last_name;

    /**
     * @var string The headline to be set, up to 100 characters
     */
    protected $headline;

    /**
     * @var string The biography to be set, up to 2500 characters
     */
    protected $bio;

    /**
     * @var string Comma-delimited list of tags for the account
     */
    protected $tags;

    /**
     * @var string Indicates whether the user can be searched by email
     */
    protected $email_searchable = 'true';

    /**
     * @var string Indicates whether the user accepts email from other users
     */
    protected $email_me = 'true';

    /**
     * @var string If true, API returns an access token
     */
    protected $login = 'true';

    /**
     * @var string Strips or includes HTML from the returned bio
     */
    protected $strip_html = 'true';

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @param string $bio
     * @return $this
     */
    public function setBio($bio)
    {
        $this->bio = $bio;

        return $this;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param bool $email_me
     * @return $this
     */
    public function setEmailMe($email_me)
    {
        $this->email_me = Functions::boolStringValue($email_me);

        return $this;
    }

    /**
     * @param bool $email_searchable
     * @return $this
     */
    public function setEmailSearchable($email_searchable)
    {
        $this->email_searchable = Functions::boolStringValue($email_searchable);

        return $this;
    }

    /**
     * @param string $first_name
     * @return $this
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;

        return $this;
    }

    /**
     * @param string $headline
     * @return $this
     */
    public function setHeadline($headline)
    {
        $this->headline = $headline;

        return $this;
    }

    /**
     * @param string $last_name
     * @return $this
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * @param bool $login
     * @return $this
     */
    public function setLogin($login)
    {
        $this->login = Functions::boolStringValue($login);

        return $this;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param string $strip_html
     * @return $this
     */
    public function setStripHtml($strip_html)
    {
        $this->strip_html = Functions::boolStringValue($strip_html);

        return $this;
    }

    /**
     * @param string|array $tags
     * @return $this
     */
    public function setTags($tags)
    {
        if (is_array($tags)) {
            $tags = implode(',', $tags);
        }

        $this->tags = $tags;

        return $this;
    }
}
