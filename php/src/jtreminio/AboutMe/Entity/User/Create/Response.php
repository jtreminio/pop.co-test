<?php

namespace jtreminio\AboutMe\Entity\User\Create;

use jtreminio\AboutMe;

class Response extends AboutMe\Entity implements AboutMe\EntityResponseInterface
{
    /**
     * @var string Status code for the call; 200 if successful
     */
    protected $status;

    /**
     * @var string Access token if login=true is provided
     */
    protected $access_token;

    /**
     * @var string Reason call failed if status is not 200
     */
    protected $error_message;

    /** @var Response\User */
    protected $user;

    public function __construct(array $values = [])
    {
        if (!empty($values['user'])) {
            $user = $values['user'];
            unset($values['user']);

            $this->user = new Response\User($user);
        }

        parent::__construct($values);
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->access_token;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->error_message;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return Response\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
