<?php

namespace jtreminio\AboutMe\Entity\User\Create\Response;

use jtreminio\AboutMe;

class User extends AboutMe\Entity
{
    /**
     * @var string About.me page URL
     */
    protected $profile;

    /**
     * @var string About.me username
     */
    protected $user_name;

    /**
     * @var string First name of the user
     */
    protected $first_name;

    /**
     * @var string Last name of the user
     */
    protected $last_name;

    /**
     * @var string First and last name values concatenated
     */
    protected $display_name;

    /**
     * @var string User’s headline text
     */
    protected $header;

    /**
     * @var string User’s biographical information
     */
    protected $bio;

    /**
     * @var string The URL to the background
     */
    protected $background;

    /**
     * @var string The URL to the background for use by a mobile application.
     *
     * The image is 80% of the original image’s resolution.
     */
    protected $mobile_background;

    /**
     * @var string The email address of the user.
     *
     * This field is only returned for when the page is for an authenticated user
     * (for example, create or login) or when the user already has the page owner’s
     * email address (search by email).
     */
    protected $email_address;

    /**
     * @var string Indicates that the user’s email address may be searched for
     */
    protected $email_searchable;

    /**
     * @var string Indicates that the user accepts email from other users
     */
    protected $email_public;

    /**
     * @var string A list of the user’s search tags (if extended=true)
     */
    protected $tags;

    /**
     * @var string A small image that displays next to the user’s biography
     */
    protected $avatar;

    /**
     * @var string Path to thumbnails
     */
    protected $img_base_url;

    /**
     * @var string A 291x187 thumbnail of the full page
     *
     * Created by about.me when the user uploads a background or edits a page
     */
    protected $thumbnail_291x187;

    /**
     * @var string An 803x408 thumbnail of the full page
     */
    protected $thumbnail1;

    /**
     * @var string A 260x176 thumbnail of the full page
     */
    protected $thumbnail2;

    /**
     * @var string A 198x134 thumbnail of the full page
     */
    protected $thumbnail3;

    /**
     * @var string A 161x109 thumbnail of the full page
     */
    protected $thumbnail4;

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @return string
     */
    public function getBackground()
    {
        return $this->background;
    }

    /**
     * @return string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->display_name;
    }

    /**
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->email_address;
    }

    /**
     * @return string
     */
    public function getEmailPublic()
    {
        return $this->email_public;
    }

    /**
     * @return string
     */
    public function getEmailSearchable()
    {
        return $this->email_searchable;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @return string
     */
    public function getImgBaseUrl()
    {
        return $this->img_base_url;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @return string
     */
    public function getMobileBackground()
    {
        return $this->mobile_background;
    }

    /**
     * @return string
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @return string
     */
    public function getThumbnail1()
    {
        return $this->thumbnail1;
    }

    /**
     * @return string
     */
    public function getThumbnail2()
    {
        return $this->thumbnail2;
    }

    /**
     * @return string
     */
    public function getThumbnail3()
    {
        return $this->thumbnail3;
    }

    /**
     * @return string
     */
    public function getThumbnail4()
    {
        return $this->thumbnail4;
    }

    /**
     * @return string
     */
    public function getThumbnail291x187()
    {
        return $this->thumbnail_291x187;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->user_name;
    }
}
