<?php

namespace jtreminio\AboutMe;

interface EntityResponseInterface
{
    public function getErrorMessage();
}
