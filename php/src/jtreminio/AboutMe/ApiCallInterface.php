<?php

namespace jtreminio\AboutMe;

interface ApiCallInterface
{
    /**
     * @return string
     */
    public function getUrl();

    /**
     * @return array
     */
    public function getData();

    /**
     * @param array $response
     * @return $this
     */
    public function parseResponse(array $response);

    /**
     * @return EntityResponseInterface
     */
    public function getResponse();

    /**
     * @return bool
     */
    public function callFailed();
}
