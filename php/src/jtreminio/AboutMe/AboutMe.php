<?php

namespace jtreminio\AboutMe;

use GuzzleHttp;

class AboutMe
{
    private $client;
    private $developerKey;

    /**
     * @param string            $developerKey
     * @param GuzzleHttp\Client $client
     * @throws \Exception
     */
    public function __construct(
        $developerKey,
        GuzzleHttp\Client $client
    ) {
        if (empty($developerKey) || !is_string($developerKey)) {
            throw new \Exception('Developer key must be set and must be a string.');
        }

        $this->client       = $client;
        $this->developerKey = $developerKey;
    }

    /**
     * @param ApiCallInterface $apiCall
     * @return EntityResponseInterface
     * @throws \Exception
     */
    public function getResponse(ApiCallInterface $apiCall)
    {
        $body = array_merge(
            $apiCall->getData(),
            ['client_id' => $this->developerKey]
        );

        try {
            $response = $this->client->post($apiCall->getUrl(), [
                'body' => $body,
            ]);
        } catch (GuzzleHttp\Exception\RequestException $e) {
            throw new \Exception('There was a problem connecting to the server.');
        }

        $apiCall->parseResponse($response->json());

        if ($apiCall->callFailed()) {
            throw new \Exception(
                'API call failed: ' . $apiCall->getResponse()->getErrorMessage()
            );
        }

        return $apiCall->getResponse();
    }
}
