<?php

namespace jtreminio\AboutMe;

abstract class Functions
{
    /**
     * Turns a string or bool value into "true" or "false"
     *
     * @param mixed $value
     * @return string
     */
    static function boolStringValue($value)
    {
        if (is_string($value)) {
            $value = strtolower(trim($value));
        }

        if ($value === 'false' || empty($value)) {
            return 'false';
        }

        return 'true';
    }
}
