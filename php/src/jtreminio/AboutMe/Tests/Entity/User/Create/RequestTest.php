<?php

namespace jtreminio\AboutMe\Tests\Entity\User\Create;

use jtreminio\AboutMe\Entity\User\Create\Request;

class RequestTest extends \PHPUnit_Framework_TestCase
{
    public function testSettingPropertiesManuallyReturnsProperResult()
    {
        $request = new Request();

        $request->setUsername('test')
            ->setEmail('test@foo.com')
            ->setPassword('123')
            ->setFirstName('First')
            ->setLastName('Last');

        $expected = [
            'username'         => 'test',
            'email'            => 'test@foo.com',
            'password'         => '123',
            'first_name'       => 'First',
            'last_name'        => 'Last',
            'headline'         => null,
            'bio'              => null,
            'tags'             => null,
            'email_searchable' => 'true',
            'email_me'         => 'true',
            'login'            => 'true',
            'strip_html'       => 'true',
        ];

        $this->assertSame($expected, $request->toArray());
    }

    public function testSettingPropertiesByArrayReturnsProperResult()
    {
        $data = [
            'username'         => 'username',
            'email'            => 'test@foo.com',
            'password'         => '123',
            'first_name'       => 'First',
            'last_name'        => 'Last',
        ];

        $expected = [
            'username'         => 'username',
            'email'            => 'test@foo.com',
            'password'         => '123',
            'first_name'       => 'First',
            'last_name'        => 'Last',
            'headline'         => null,
            'bio'              => null,
            'tags'             => null,
            'email_searchable' => 'true',
            'email_me'         => 'true',
            'login'            => 'true',
            'strip_html'       => 'true',
        ];

        $request = (new Request())
            ->fromArray($data);

        $this->assertSame($expected, $request->toArray());
    }
}
