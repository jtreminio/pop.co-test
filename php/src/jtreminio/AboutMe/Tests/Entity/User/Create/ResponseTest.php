<?php

namespace jtreminio\AboutMe\Tests\Entity\User\Create;

use jtreminio\AboutMe\Entity\User\Create\Response;

class ResponseTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructDoesNotSetUserIfValueEmpty()
    {
        $responseValues = [
            'status'       => '200',
            'access_token' => 'accessToken',
            'user'         => [],
        ];

        $response = new Response($responseValues);

        $this->assertEmpty($response->getUser());
    }

    public function testPassingParameterToConstructorPopulatesUserObject()
    {
        $responseValues = [
            'status'       => '200',
            'access_token' => 'accessToken',
            'user'         => [
                'profile'   => 'http://about.me/test',
                'user_name' => 'test',
            ],
        ];

        $response = new Response($responseValues);

        $this->assertEquals(
            $responseValues['user']['profile'],
            $response->getUser()->getProfile()
        );

        $this->assertEquals(
            $responseValues['user']['user_name'],
            $response->getUser()->getUserName()
        );
    }
}
