<?php

namespace jtreminio\AboutMe\Tests\Api\User;

use jtreminio\AboutMe\ApiCall;
use jtreminio\AboutMe\Entity;

class CreateTest extends \PHPUnit_Framework_TestCase
{
    public function testGetUrlReturnsProperlyCreatedString()
    {
        $entity = new Entity\User\Create\Request();

        $entity->setUsername('test')
            ->setEmail('test@foo.com')
            ->setPassword('123')
            ->setFirstName('First')
            ->setLastName('Last');

        $api = new ApiCall\User\Create($entity);

        $expected = 'https://api.about.me/api/v2/json/user/create/test';

        $this->assertSame($expected, $api->getUrl());
    }

    public function testGetDataFiltersInvalidApiProperty()
    {
        $entity = new Entity\User\Create\Request();

        $entity->setUsername('test')
            ->setEmail('test@foo.com')
            ->setPassword('123')
            ->setFirstName('First')
            ->setLastName('Last');

        $api = new ApiCall\User\Create($entity);

        $expected = [
            'email'            => 'test@foo.com',
            'password'         => '123',
            'first_name'       => 'First',
            'last_name'        => 'Last',
            'headline'         => null,
            'bio'              => null,
            'tags'             => null,
            'email_searchable' => 'true',
            'email_me'         => 'true',
            'login'            => 'true',
            'strip_html'       => 'true',
        ];

        $this->assertSame($expected, $api->getData());
    }
}
