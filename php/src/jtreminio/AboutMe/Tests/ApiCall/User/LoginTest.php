<?php

namespace jtreminio\AboutMe\Tests\Api\User;

use jtreminio\AboutMe\ApiCall;
use jtreminio\AboutMe\Entity;

class LoginTest extends \PHPUnit_Framework_TestCase
{
    public function testGetUrlReturnsProperlyCreatedString()
    {
        $entity = new Entity\User\Login\Request();

        $entity->setUsername('loginTestUsername')
            ->setPassword('123')
            ->setRequestid('321');

        $api = new ApiCall\User\Login($entity);

        $expected = 'https://api.about.me/api/v2/json/user/login/loginTestUsername';

        $this->assertSame($expected, $api->getUrl());
    }

    public function testGetDataFiltersInvalidApiProperty()
    {
        $entity = new Entity\User\Login\Request();

        $entity->setUsername('loginTestUsername')
            ->setPassword('123')
            ->setRequestid('321');

        $api = new ApiCall\User\Login($entity);

        $expected = [
            'grant_type'   => 'password',
            'scope'        => 'general',
            'password'     => '123',
            'show_profile' => 'false',
            'strip_html'   => 'true',
            'requestid'    => '321',
        ];

        $this->assertSame($expected, $api->getData());
    }
}
