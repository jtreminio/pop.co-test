<?php

namespace jtreminio\AboutMe\Tests;

use jtreminio\AboutMe\AboutMe;
use jtreminio\AboutMe\ApiCall;
use jtreminio\AboutMe\Entity;

use GuzzleHttp;

class AboutMeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @param mixed $developerKey
     * @dataProvider providerAboutMeConstructThrowsExceptionWhenDeveloperKeyEmpty
     */
    public function testAboutMeConstructThrowsExceptionWhenDeveloperKeyEmpty($developerKey)
    {
        $this->setExpectedException(
            'Exception',
            'Developer key must be set and must be a string.'
        );

        $guzzle = $this->getMockBuilder(GuzzleHttp\Client::class)
            ->getMock();

        $aboutMe = new AboutMe($developerKey, $guzzle);
    }

    public function providerAboutMeConstructThrowsExceptionWhenDeveloperKeyEmpty()
    {
        return [
            [''],
            [[]],
            [123]
        ];
    }

    public function testGetResponseAddsClientIdToBody()
    {
        $guzzle = $this->getMockBuilder(GuzzleHttp\Client::class)
            ->getMock();

        $guzzleResponse = $this->getMockBuilder(GuzzleHttp\Message\ResponseInterface::class)
            ->getMock();

        $requestData = [
            'username'   => 'jtremin1popco',
            'email'      => 'jtreminio1@pop.co',
            'password'   => 'gsiGSRIG92NI2Gg@(JG24IN',
            'first_name' => 'Juan',
            'last_name'  => 'Treminio',
        ];

        $request = new Entity\User\Create\Request($requestData);

        $apiCall = new ApiCall\User\Create($request);

        $expectedBody = [
            'body' => [
                'client_id'        => 'developer_key',
                'email'            => 'jtreminio1@pop.co',
                'password'         => 'gsiGSRIG92NI2Gg@(JG24IN',
                'first_name'       => 'Juan',
                'last_name'        => 'Treminio',
                'headline'         => null,
                'bio'              => null,
                'tags'             => null,
                'email_searchable' => 'true',
                'email_me'         => 'true',
                'login'            => 'true',
                'strip_html'       => 'true',
            ],
        ];

        $guzzle->expects($this->once())
            ->method('post')
            ->with($apiCall->getUrl(), $expectedBody)
            ->will($this->returnValue($guzzleResponse));

        $guzzleResponse->expects($this->once())
            ->method('json')
            ->will($this->returnValue([]));

        $aboutMe = new AboutMe(
            'developer_key',
            $guzzle
        );

        $aboutMe->getResponse($apiCall);
    }

    public function testGetResponseThrowsStandardMessageOnGuzzleException()
    {
        $this->setExpectedException(
            'Exception',
            'There was a problem connecting to the server.'
        );

        $guzzle = $this->getMockBuilder(GuzzleHttp\Client::class)
            ->getMock();

        $requestInterface = $this->getMockBuilder(GuzzleHttp\Message\RequestInterface::class)
            ->getMock();

        $requestData = [
            'username'   => 'jtremin1popco',
            'email'      => 'jtreminio1@pop.co',
            'password'   => 'gsiGSRIG92NI2Gg@(JG24IN',
            'first_name' => 'Juan',
            'last_name'  => 'Treminio',
        ];

        $request = new Entity\User\Create\Request($requestData);

        $apiCall = new ApiCall\User\Create($request);

        $exception = new GuzzleHttp\Exception\RequestException(
            '', $requestInterface
        );

        $guzzle->expects($this->once())
            ->method('post')
            ->will($this->throwException($exception));

        $aboutMe = new AboutMe(
            'developer_key',
            $guzzle
        );

        $aboutMe->getResponse($apiCall);
    }
}
