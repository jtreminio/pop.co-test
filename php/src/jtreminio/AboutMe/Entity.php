<?php

namespace jtreminio\AboutMe;

abstract class Entity
    extends Entity\ArraySerializableAbstract
    implements Entity\ArraySerializable
{
    public function __construct(array $values = [])
    {
        if (!empty($values)) {
            $this->fromArray($values);
        }
    }
}
