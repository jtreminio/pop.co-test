* Create a class or library which utilizes the About.me API (http://about.me/developer/api/docs/) and implements
  the following two scenarios:
    * Handle the dynamic creation of a new user (http://about.me/developer/api/docs/#create)
        * Ensure the user data returned from the API is included in the success response
    * Handle the login (single sign-on) of a previously created user (http://about.me/developer/api/docs/#login)
        * Either handle redirection or include the redirection url in the method response
* Make sure to utilize ideal cURL behavior; whether that be a third party request client or something custom rolled
* Make sure to account for and handle API errors gracefully
    * Given the small scope of the class, unit testing and response mocking is encouraged
* MySQL/SQLite/Redis/Memcache are sufficient for storing data for the demo - we understand that you'd normally be
  working within an extensive ORM or well-written model library.
* Make sure to include an example usage file


Example usage
===============

Login
----------

    $aboutMe = new AboutMe(
        'api_key',
        new GuzzleHttp\Client()
    );

    $request = new Entity\User\Login\Request();
    $request->setUsername('username')
        ->setPassword('password');

    $apiCall = new ApiCall\User\Login($request);
    $response = $aboutMe->getResponse($apiCall);

    $emailAddress = $apiCall->getResponse()
        ->getUserInfo()
        ->getEmailAddress();

    foreach ($apiCall->getResponse()->getUserInfo()->getWebsites() as $website) {
        $website->getDisplayName();
    }

Create
----------
    $request = new Entity\User\Create\Request([
        'username'   => 'jtremin10popco',
        'email'      => 'jtreminio+popco10@gmail.com',
        'password'   => 'gsiGSRIG92NI2Gg',
        'first_name' => 'Juan',
        'last_name'  => 'Treminio',
    ]);

    $apiCall = new ApiCall\User\Create($request);
    $response = $aboutMe->getResponse($apiCall);

    $token = $apiCall->getResponse()
        ->getAccessToken();

    $header = $apiCall->getResponse()
        ->getUser()
        ->getHeader();
